import logging
import click
import sys
import json
import numpy as np
from scipy import signal
from assimulo.problem import Explicit_Problem
from assimulo.solvers.sundials import CVode
import matplotlib.pyplot as plt

# to import the library
sys.path.insert(0, '../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import BiolabHelper

# Sets random number generator
np.random.seed(12345678)

logging.basicConfig(level=logging.INFO)


def main():
    # ---------------------------------- GET DATA ---------------------------------------------------------
    t0 = 0
    tf = 40

    logging.info("Initializing bioreactor parameters")
    model_parameters1 = {
        "kS": 0.02,
        "mu_max": 0.4,
        "YXS": 0.5,
        "YPX": 0.2,
        "cSF": 500.0,
        "mu_set": 0.15,
        "tF": 8.0,
        "VL_max": 2.5
    }

    model_parameters2 = {
        "kS": 0.02,
        "mu_max": 0.42,
        "YXS": 0.5,
        "YPX": 0.25,
        "cSF": 500.0,
        "mu_set": 0.15,
        "tF": 8.0,
        "VL_max": 2.5
    }

    measurements = {
        "samples": [{
            'P': 0.0,
            'S': 40.0,
            'VL': 1.0,
            'X': 0.1,
            'TP': 0.0
        }]
    }

    x0 = [measure for key, measure in measurements['samples'][0].items() if key != 'TP']

    # ---------------------------------- SIMULATION ------------------------------------------------------

    logging.info("Running simulation...")
    results1 = ExponentialFedBatch().execute(x0=x0, t=[t0, tf], model_parameters=model_parameters1,
                                             initial_values=measurements['samples'][0])
    results2 = ExponentialFedBatch().execute(x0=x0, t=[t0, tf], model_parameters=model_parameters2,
                                             initial_values=measurements['samples'][0])

    logging.info("Simulation finished")

    measurements_name = ["P", "S", "VL", "X"]
    measurements['samples'] = []

    for index, measure in enumerate(measurements_name):
        plt.figure(f"Measure '{measure}'")
        plt.plot(results1[0], np.transpose([results1[1][:, index]]), label='In-silico')
        plt.plot(results2[0], np.transpose([results2[1][:, index]]), label="Estimated")
        plt.title(f"Measure '{measure}'")
        plt.xlabel("Time [min]")
        plt.ylabel("Concentration (g/l)")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    main()
