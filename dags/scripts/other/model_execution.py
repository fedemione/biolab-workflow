import logging
import click
import sys
import json
import numpy as np
from scipy import signal
from assimulo.problem import Explicit_Problem
from assimulo.solvers.sundials import CVode
import matplotlib.pyplot as plt

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.lib.biolab_functions import BiolabHelper

# Sets random number generator
np.random.seed(12345678)

logging.basicConfig(level=logging.INFO)


# Defines the model class
class ExponentialFedBatch(Explicit_Problem):

    def rhs(self, t, y):

        # Unpacks the state vector. The states are alphabetically ordered.
        P, S, VL, X = y

        # Unpacks the model parameters.
        YPX = self.model_parameters['YPX']
        YXS = self.model_parameters['YXS']
        cSF = self.model_parameters['cSF']
        mu_set = self.model_parameters['mu_set']
        tF = self.model_parameters['tF']
        VL_max = self.model_parameters['VL_max']

        # For calculation of F, these two initial values are needed
        S0 = self.initial_values['S']
        VL0 = self.initial_values['VL']

        # Calculate the current specific rates
        mu = self.growth_rate(y)
        qS = 1 / YXS * mu
        qP = YPX * mu

        # Calculate the feeding profile, conditional to the corresponding events
        if t > tF and not VL > VL_max:
            # square = (2 + signal.square(2 * np.pi / 5 * (t - tF), 0.1))
            # square = (1 + signal.square(2 * np.pi / 5 * (t - tF), 0.1)) / 2
            square = 1
            profile = (S0 * VL0 * mu_set) / (cSF - S) * np.exp(mu_set * (t - tF))
            F = square * profile
        else:
            F = 0.0

        # Calculate state derivatives
        dXdt = mu * X - F / VL * X
        dSdt = -qS * X + F / VL * (cSF - S)
        dPdt = qP * X - F / VL * P
        dVLdt = F

        # Return list of state derivatives in the same order as the state vector was unpacked
        return [dPdt, dSdt, dVLdt, dXdt]

    # The Monod equation is defined as instance method
    def growth_rate(self, y):
        P, S, VL, X = y
        mu_max = self.model_parameters['mu_max']
        kS = self.model_parameters['kS']
        mu = mu_max * S / (kS + S)
        return mu

    def execute(self, x0, t, model_parameters, initial_values):
        # actualiza valores de parametros y acciones para la ejecución actual
        self.t0 = t[0]
        self.y0 = x0
        self.model_parameters = model_parameters
        self.initial_values = initial_values

        simulator = CVode(self)
        simulator.verbosity = 10
        result = simulator.simulate(tfinal=t[1], ncp=100)
        return result


def main():
    # ---------------------------------- GET DATA ---------------------------------------------------------
    t0 = 0
    # tf = 2700
    tf = 40
    mbr = "mbr"

    helper = BiolabHelper()

    logging.info("Initializing bioreactor parameters")
    model_parameters = {
        "parameters_real": {
            "kS": 0.02,
            "mu_max": 0.4,
            "YXS": 0.5,
            "YPX": 0.2,
            "cSF": 500.0,
            "mu_set": 0.15,
            "tF": 8.0,
            "VL_max": 2.5
        }
    }
    measurements = {
        "samples": [{
            'P': 0.0,
            'S': 40.0,
            'VL': 1.0,
            'X': 0.1,
            'TP': 0.0
        }]
    }

    # Init structure to save results
    results_modify = {"X": [], "P": [], "S": [], "VL": []}
    time = []

    samples = range(0, 40, 4)

    # Modify params (apply coeff) from -30% to +30%. There are 7 simulations
    modify_param = {"mu_set": [1.02, 1.01, 1.03, 0.96, 1.0, 1.04, 0.97, 0.99, 0.98]}
    mbrs = ["mbr11", "mbr12", "mbr13", "mbr21", "mbr22", "mbr23", "mbr31", "mbr32", "mbr33"]

    for index_mbr, param in enumerate(modify_param['mu_set']):
        model_parameters_modify = model_parameters['parameters_real'].copy()
        model_parameters_modify['mu_set'] = model_parameters_modify['mu_set'] * param

        x0 = [measure for key, measure in measurements['samples'][0].items() if key != 'TP']

        # ---------------------------------- LOGGING ---------------------------------------------------------

        logging.info("modify coefficient:  %s", modify_param)
        logging.info("t0:  %s", t0)
        logging.info("tf:  %s", tf)
        logging.info("mbr:  %s", mbr)
        logging.info("initial measures:")
        logging.info(measurements['samples'][0])
        logging.info("model parameters:")
        logging.info(model_parameters['parameters_real'])

        # ---------------------------------- SIMULATION ------------------------------------------------------

        logging.info("Running simulation...")
        results = ExponentialFedBatch().execute(x0=x0, t=[t0, tf],
                                                model_parameters=model_parameters_modify,
                                                initial_values=measurements['samples'][0])

        # time = results[0]
        # results_modify["P"].append(results[1][:, 0])
        # results_modify["S"].append(results[1][:, 1])
        # results_modify["VL"].append(results[1][:, 2])
        # results_modify["X"].append(results[1][:, 3])
        logging.info("Simulation finished")

        measurements_name = ["P", "S", "VL", "X", "TP"]
        measurements['samples'] = []
        for sample in samples:
            time_index = results[0].index(sample)
            measurements['samples'] = list(np.concatenate((measurements['samples'], [dict(zip(measurements_name, np.concatenate((results[1][time_index],[sample]))))])))
        helper.save_measurements(mbrs[index_mbr], measurements)

    with plt.style.context('ggplot'):
        for key, value in dict({"P": "Product", "S": "Glucose", "X": "Biomass", "VL": "Volume"}).items():
            plt.figure(value)
            plt.title("Simulation with mu_set matrix [{}]".format(value), fontsize=16)
            plt.plot(time, np.transpose(results_modify[key]), label=modify_param["mu_set"])
            plt.legend()
            # plt.show()

        # t = np.arange(0, 300, 0.1)
        # tF = 8.0
        # square = (2 + signal.square(2 * np.pi / 5 * (t - tF), 0.1))
        # # square = (1 + signal.square(2 * np.pi / 5 * (t - tF), 0.1)) / 2
        # # square = 1
        # plt.figure("Pulse")
        # plt.title("Simulation with exponential feed pulses".format(value), fontsize=16)
        # plt.plots(t, square)
        # plt.show()

    # To plots only one simulation
    # with plt.style.context('ggplot'):
    #     fig, axs = plt.subplots(2, 2)
    #     fig.suptitle("Simulation with exponential pulse feeding", fontsize=16)
    #     axs[0, 0].plots(results[0], results[1][:, 0], 'red', label="P")
    #     axs[0, 0].legend()
    #     axs[0, 1].plots(results[0], results[1][:, 1], 'blue', label="S")
    #     axs[0, 1].legend()
    #     axs[1, 0].plots(results[0], results[1][:, 2], 'green', label="VL")
    #     axs[1, 0].legend()
    #     axs[1, 1].plots(results[0], results[1][:, 3], 'orange', label="X")
    #     axs[1, 1].legend()
    #     plt.show()


if __name__ == "__main__":
    main()
