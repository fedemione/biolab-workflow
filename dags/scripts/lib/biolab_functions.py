import os
import json
from pathlib import Path
import sys

sys.path.insert(0, '../../..')
from scripts.lib.monitoring import Monitoring


class BiolabHelper:

    def __init__(self, dag_id=None):
        self.measurements_file = f"../../data/samples/{{}}.json"
        self.params_file = f"../../data/params/params.json"
        self.plot_file = f"../../data/plots/"
        self.config_file = f"../../dags_examples/{dag_id}/config.json"

    def read_config(self, item):
        _dir = os.path.join(os.path.dirname(__file__), self.config_file)
        with open(_dir) as file:
            data = json.load(file)
        return data[item]

    # TODO: rename
    def calc_times(self):
        # Delta time between pulses of feed, equal for all MBRs
        delta_t_feed = self.read_config('FEED')['DELTA_TIME']
        # Get number of samples from config file
        sample_number = self.read_config('SAMPLES')['QUANTITY']
        # Batch Time
        tf = self.read_config('MODEL_PARAMETERS_PRIORI')['tF']
        # Create of times Feed and times Samples
        t_pulse = []
        t_sample = []
        # Time between Feed pulse and Sample given for the block1, It's 3/10 of delta_t_Feed
        # 3/10: 3 block tasks over 10
        t_between_fs = delta_t_feed * 3 / 10
        # The delta time between samples is equivalent to 2 times of feed. It mean, each 2 feeds 1 sample is done
        delta_t_sample = delta_t_feed * 2
        for i in range(int(sample_number)):
            # The first elements are affected by tF (batch time), the rest of the times just add delta times
            t_pulse.append(tf) if i == 0 else t_pulse.append(t_pulse[i - 1] + delta_t_feed)
            t_sample.append(t_pulse[i] + delta_t_feed + t_between_fs) if i == 0 else t_sample.append(
                t_sample[i - 1] + delta_t_sample)
        return t_pulse, t_sample

    def init_params(self):
        # Read config file
        params = {
            'parameters_estimated': [self.read_config('MODEL_PARAMETERS_PRIORI')],
            'parameters_real': self.read_config('MODEL_PARAMETERS_REAL')
        }

        # Write output.
        output_path = self.check_path(self.params_file)
        Monitoring("Start").clear_file().save_file("Initialize the parameters json file", output_path)
        with open(output_path, "w+", newline='') as file:
            json.dump(params, file, ensure_ascii=False, indent=2)

    def init_measurements(self, mbr):
        # Read config file
        measurement = {}
        initial_measurement = self.read_config('INITIAL_STATES')

        for k, v in initial_measurement.items():
            measurement[k[:-1]] = v

        json_data = {'samples': [measurement], "params": [{}]}

        # Write output
        output_path = self.check_path(self.measurements_file.format(mbr))
        Monitoring(f"Init {mbr}").save_file(f"Initialize the measures json file for the {mbr}", output_path)
        with open(output_path, "w+", newline='') as file:
            json.dump(json_data, file, ensure_ascii=False, indent=2)

    def get_measurements(self, mbr):
        file_path = os.path.join(os.path.dirname(__file__), self.measurements_file.format(mbr))
        with open(file_path, 'r', encoding='utf-8') as f:
            json_content = json.load(f)
        return json_content

    def get_params(self):
        file_path = os.path.join(os.path.dirname(__file__), self.params_file)
        with open(file_path, 'r', encoding='utf-8') as f:
            json_content = json.load(f)
        return json_content

    def get_config(self):
        with open(self.config_file) as file:
            data = json.load(file)
        return data

    def save_measurements(self, mbr, measurements):
        output_path = self.check_path(self.measurements_file.format(mbr))
        with open(output_path, 'w+', encoding='utf-8') as f:
            json.dump(measurements, f, ensure_ascii=False, indent=2)

    def save_params(self, params):
        output_path = self.check_path(self.params_file)
        with open(output_path, 'w+', encoding='utf-8') as f:
            json.dump(params, f, ensure_ascii=False, indent=2)

    def check_path(self, _dir):
        file = os.path.join(os.path.dirname(__file__), _dir)
        output_path = Path(file)
        output_dir = output_path.parent
        output_dir.mkdir(parents=True, exist_ok=True)
        return output_path
