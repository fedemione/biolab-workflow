import os
from pathlib import Path


class Monitoring:

    def __init__(self, node):
        """
        Function to initialize the Monitoring class
        :param node: Node responsible for saving data
        """
        self.path = "../../data/monitoring/monitoring.txt"
        self.message = ""
        self.node = node

    def clear_file(self):
        """
        Clear file to init new workflow
        :return: Monitoring instance
        """
        monitoring_path = self.get_file_path()
        open(monitoring_path, 'w').close()
        return self

    def get_file_path(self):
        """
        Function to return the file path defined for monitoring
        :return: File Path
        """
        file = os.path.join(os.path.dirname(__file__), self.path)
        output_path = Path(file)
        output_dir = output_path.parent
        output_dir.mkdir(parents=True, exist_ok=True)
        return output_path

    def save_message(self, task, params=""):
        """
        Function to save in a txt file
        :param task:
        :param params:
        """
        message = f"[{task}] - {params}\n"
        print(message)
        self.message = f"{self.message}{message}"

    def save_file(self, task=None, params=None, erase=False):
        """
        Function to save messages stored locally into the monitoring file
        :param task: value set to save one time message
        :param params: value set to save one time message
        :param erase: Option to override local messages
        """
        monitoring_path = self.get_file_path()
        with open(monitoring_path, 'a') as file_object:
            # # Set file title if it is empty
            # if len(file_object.read(10)) == 0:
            #     file_object.write("-----------------------------------------------------------\n")
            #     file_object.write("Log file for the entire Experimental-Computational Workflow\n")
            #     file_object.write("-----------------------------------------------------------\n\n")

            file_object.write(f"-------------- [NODE: {self.node}] --------------\n")
            if task and params:
                message = f"[{task}] - {params}\n"
                print(message)
                self.message = message if erase else f"{self.message}\n{message}"
            file_object.write(f"{self.message}\n\n")
            file_object.close()
