import logging
from bayes_opt import BayesianOptimization
import sys
import numpy as np
import matplotlib.pyplot as plt

# to import the library biolab_functions
sys.path.insert(0, '../..')

from scripts.models.assimulo.model_execution import ExponentialFedBatch

logging.basicConfig(level=logging.INFO)


def bayesian_optimization(initial_values, tf):
    # objetive: maximize biomass concentration
    def objective_function(tfeed, mu_set):

        model_parameters = {
            "kS": 0.02,
            "mu_max": 0.4,
            "YXS": 0.5,
            "YPX": 0.2,
            "cSF": 500.0,
            "mu_set": mu_set,
            "tF": tfeed,
            "VL_max": 2.5
        }

        logging.info("Running estimated simulation...")
        x0 = [v for k, v in initial_values.items() if k != 'TP']
        try:
            states = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=model_parameters,
                                                   initial_values=initial_values)
        except:
            logging.info("[Bayesian Optimization] - Error on model execution - continue")
            return -1
        # get time to reach maximum product
        index_max = np.argmax(np.transpose(states[1])[0])
        max_p = states[1][-1][0]
        time_reached_max_p = states[0][index_max]
        print(max_p, time_reached_max_p)

        return max_p * (tf - time_reached_max_p)

    pbounds = {
        'tfeed': (0, 20),
        'mu_set': (0, 1),
    }

    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=pbounds,
        random_state=1,
        verbose=2
    )

    optimizer.maximize(
        init_points=5,
        n_iter=50,
        acq="ucb",
        kappa=3
    )

    logging.info("Optimizer MAX: {}".format(optimizer.max))
    print("Optimizer MAX: {}".format(optimizer.max))

    x0 = [v for k, v in initial_values.items() if k != 'TP']
    model_parameters = {
        "kS": 0.02,
        "mu_max": 0.4,
        "YXS": 0.5,
        "YPX": 0.2,
        "cSF": 500.0,
        "mu_set": optimizer.max["params"]["mu_set"],
        "tF": optimizer.max["params"]["tfeed"],
        "VL_max": 2.5
    }
    best_simulation = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=model_parameters,
                                                    initial_values=initial_values)

    with plt.style.context('ggplot'):

        # --------------------------- Plot estimated vs in-silico ---------------------------
        measurements_name = [key for key in initial_values if key != 'TP']
        for index, measure in enumerate(measurements_name):
            plt.figure("Measure '{}'".format(measure))
            plt.plot(best_simulation[0], np.transpose([best_simulation[1][:, index]]), label='Estimated')
            plt.title("Measure '{}'".format(measure))
            plt.xlabel("Time [min]")
            plt.ylabel("Concentration (g/l)")
            plt.legend()
            plt.show()
        # -----------------------------------------------------------------------------------
        x = []
        y = []
        z = []
        for step in optimizer.res:
            if step["target"] > 0:
                x.append(step["params"]["mu_set"])
                y.append(step["params"]["tfeed"])
                z.append(step["target"])

        x = np.array(x)
        y = np.array(y)
        z = np.array(z)

        plt.figure()
        ax = plt.axes(projection='3d')

        ax.plot_trisurf(x, y, z, cmap='viridis', edgecolor='none')
        ax.set_title('Surface plots')
        plt.show()

        plt.figure("MuSet VS MaxTarget")
        plt.scatter(x, z)
        plt.title("MuSet VS MaxTarget")
        plt.xlabel("muset")
        plt.ylabel("target")
        plt.legend()
        plt.show()

        plt.figure("TFeed VS MaxTarget")
        plt.scatter(y, z)
        plt.title("TFeed VS MaxTarget")
        plt.xlabel("tfeed")
        plt.ylabel("target")
        plt.legend()
        plt.show()

    return 1


def main():
    bayesian_optimization(
        initial_values={
            'P': 0.0,
            'S': 40.0,
            'VL': 1.0,
            'X': 0.1
        },
        tf=45.0
    )


if __name__ == "__main__":
    main()
