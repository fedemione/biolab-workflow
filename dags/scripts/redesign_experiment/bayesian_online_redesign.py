import numpy as np
import click
import json
import sys
from bayes_opt import BayesianOptimization

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.parameter_update.variational_inference import VariationalInference
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring


def bayesian_optimization(initial_values, model_parameters, bounds, tf):
    # objetive: maximize biomass concentration
    def objective_function(**params):

        # check if model parameters are probabilistics or deterministics
        if isinstance(list(model_parameters.values())[0], list):
            svi = VariationalInference(prior=model_parameters)
            params_sample = svi.sample_param()

        for param in params:
            params_sample[param] = params[param]

        print("Running estimated simulation...")
        x0 = [val for key, val in initial_values.items() if key != 'TP']
        try:
            #TODO: try to implement from current state to final time (but we have 9 MBRs, so maybe is it imposible, and this approach it is correct)
            states = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=params_sample,
                                                   initial_values=initial_values)
        except:
            print("[Bayesian Optimization] - Error on model execution - continue")
            return 0
        return states[1][-1][0]

    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=bounds,
        random_state=1,
        verbose=2
    )

    optimizer.maximize(
        init_points=2,
        n_iter=12,
        acq="ucb",
        kappa=3
    )

    return optimizer.max["params"]


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--tf",
    type=click.STRING,
    required=True,
    help="Final time",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, tf, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    # Initialize monitoring object
    monitoring = Monitoring("Online Redesign")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    monitoring.save_message("Initializing values and bioreactor parameters")
    MBRs = json.loads(mbrs)
    bounds = json.loads(unknows)
    model_parameters = helper.get_params()
    measurements = helper.get_measurements(MBRs[0])

    # ---------------------------------- LOGGING ---------------------------------------------------------

    monitoring.save_message("Parameter bounds", bounds)
    monitoring.save_message("Current parameters values", model_parameters['parameters_estimated'][-1])

    # --------------------------- BAYESIAN ONLINE REDESIGN -----------------------------------------------

    monitoring.save_message("Initializing redesign")
    optimal_params = bayesian_optimization(
        initial_values=measurements['samples'][0],
        model_parameters=model_parameters['parameters_estimated'][-1],
        bounds=bounds,
        tf=float(tf)
    )

    monitoring.save_message("Online Redesign Finished")

    # --------------------------------- SAVE RESULTS ------------------------------------------------------

    # fix decimal length
    optimal_params = {k: round(v, 3) for k, v in optimal_params.items()}

    # replace new estimations
    model_parameters['parameters_estimated'][-1].update(optimal_params.items())

    # save new params
    helper.save_params(model_parameters)
    monitoring.save_message("Save parameters redesign to json file", optimal_params.items())
    monitoring.save_file()


if __name__ == "__main__":
    main()
