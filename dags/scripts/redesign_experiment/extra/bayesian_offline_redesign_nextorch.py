import numpy as np
from nextorch import bo, doe
from nextorch.parameter import Parameter, ParameterSpace
import sys

sys.path.insert(0, '../..')

from scripts.models.assimulo.model_execution import ExponentialFedBatch


def bayesian_optimization(initial_values, model_parameters, tf):
    def objective_function(x):

        y = []

        for xi in x:
            model_parameters.update({"mu_set": xi[1], "tF": xi[0]})
            x0 = [v for k, v in initial_values.items() if k != 'TP']
            try:
                states = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=model_parameters,
                                                       initial_values=initial_values)
            except:
                print("[Bayesian Optimization] - Error on model execution - continue")
                return [-1, -1]

            # get time to reach maximum product
            index_max = np.argmax(np.transpose(states[1])[0])
            max_p = states[1][-1][0]
            time_reached_max_p = states[0][index_max]
            y.append([max_p, tf - time_reached_max_p])

        return y


    pbounds = {
        'tF': [0, 20],
        'mu_set': [0, 1],
    }

    parameter_1 = Parameter(name="tF", x_type='continuous', x_range=pbounds['tF'])
    parameter_2 = Parameter(name="mu_set", x_type='continuous', x_range=pbounds['mu_set'])
    parameter_space = ParameterSpace([parameter_1, parameter_2])
    X_ranges = parameter_space.X_ranges

    # Get the information of the design space
    n_dim = len(X_ranges)  # the dimension of inputs

    # --------------------------------- Initial Sampling -----------------------------------------------
    # Latin hypercube design with 10 initial points
    n_init_lhc = 5
    X_init_lhc = doe.latin_hypercube(n_dim=n_dim, n_points=n_init_lhc, seed=1)
    # Get the initial responses
    Y_init_lhc = bo.eval_objective_func(X_init_lhc, X_ranges, objective_function)

    # -------------------- Initialize an multi-objective Experiment object -----------------------------
    # Set its name, the files will be saved under the folder with the same name
    Exp_lhc = bo.WeightedMOOExperiment('PFR_yield_MOO')
    # Import the initial data
    Exp_lhc.input_data(X_init_lhc,
                       Y_init_lhc,
                       X_names=["ghk", "ljkhl"],
                       X_ranges=X_ranges,
                       unit_flag=True)

    # Set a weight vector for objective 1
    weights_obj_1 = 0.5
    weights_obj_2 = 0.5

    Exp_lhc.set_optim_specs(objective_func=objective_function,
                            maximize=True,
                            weights=weights_obj_1)

    # Set the number of iterations for each experiments
    n_trials_lhc = 25
    Exp_lhc.run_exp_auto(n_trials_lhc)

    # Extract the set of optimal solutions
    Y_real_opts, X_real_opts = Exp_lhc.get_optim()
    print(Y_real_opts, X_real_opts)


def main():
    bayesian_optimization(
        initial_values={
            'P': 0.0,
            'S': 40.0,
            'VL': 1.0,
            'X': 0.1
        },
        model_parameters={
            "kS": 0.02,
            "mu_max": 0.4,
            "YXS": 0.5,
            "YPX": 0.2,
            "cSF": 500.0,
            "mu_set": 0.15,
            "tF": 8.0,
            "VL_max": 2.5
        },
        tf=45.0
    )


if __name__ == "__main__":
    main()
