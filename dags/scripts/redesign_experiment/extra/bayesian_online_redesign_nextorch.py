import numpy as np
import click
import json
import sys
from nextorch import bo, doe
from nextorch.parameter import Parameter, ParameterSpace

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.parameter_update.variational_inference import VariationalInference
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring


def bayesian_optimization(initial_values, model_parameters, unknows, tf):
    # objetive: maximize biomass concentration
    def objective_function(x):

        y = []

        for xi in x:

            # check if model parameters are distributions or determined
            if isinstance(list(model_parameters.values())[0], list):
                svi = VariationalInference(prior=model_parameters)
                params_sample = svi.sample_param()

            for param in xi:
                params_sample[param] = params[param]

            print("Running estimated simulation...")
            x0 = [v for k, v in initial_values.items() if k != 'TP']

            try:
                # TODO: check t0 = 0. Must be the current time, projected to the end of the experiment (tf) with the current state (X, S, P, VL)
                states = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=params_sample,
                                                       initial_values=initial_values)
            except:
                print("[Bayesian Optimization] - Error on model execution - continue")
                return [-1, -1]

            # get time to reach maximum product
            index_max = np.argmax(np.transpose(states[1])[0])
            max_p = states[1][-1][0]
            time_reached_max_p = states[0][index_max]
            y.append([max_p, tf - time_reached_max_p])

        return y


    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=unknows,
        random_state=1,
        verbose=2
    )

    optimizer.maximize(
        init_points=2,
        n_iter=12,
        acq="ucb",
        # TODO: check kappa. Could be 2 (2 * std deviation)
        kappa=3
    )

    return optimizer.max["params"]


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--tf",
    type=click.STRING,
    required=True,
    help="Final time",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, tf, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    # Initialize monitoring object
    monitoring = Monitoring("Online Redesign")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    monitoring.save_message("Initializing values and bioreactor parameters")
    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    model_parameters = helper.get_params()
    measurements = helper.get_measurements(MBRs[0])

    # ---------------------------------- LOGGING ---------------------------------------------------------

    monitoring.save_message("Parameter bounds", _unknows)
    monitoring.save_message("Current parameters values", model_parameters['parameters_estimated'][-1])

    # --------------------------- BAYESIAN ONLINE REDESIGN -----------------------------------------------

    monitoring.save_message("Initializing redesign")
    optimal_params = bayesian_optimization(
        # TODO: check t0 = 0. Must be the current time, projected to the end of the experiment (tf) with the current state (X, S, P, VL)
        initial_values=measurements['samples'][0],
        model_parameters=model_parameters['parameters_estimated'][-1],
        unknows=_unknows,
        tf=float(tf)
    )

    monitoring.save_message("Online Redesign Finished")

    # --------------------------------- SAVE RESULTS ------------------------------------------------------

    # fix decimal length
    optimal_params = {k: round(v, 3) for k, v in optimal_params.items()}

    # replace new estimations
    model_parameters['parameters_estimated'][-1].update(optimal_params.items())

    # save new params
    helper.save_params(model_parameters)
    monitoring.save_message("Save parameters redesign to json file", optimal_params.items())
    monitoring.save_file()


if __name__ == "__main__":
    main()
