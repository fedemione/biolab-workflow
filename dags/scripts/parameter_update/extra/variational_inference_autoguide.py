import sys
import json
import numpy as np
import click
import torch
import torch.distributions.constraints as constraints
import pyro
import matplotlib.pyplot as plt
from pyro.optim import Adam, ClippedAdam
from pyro.infer import SVI, Trace_ELBO
from pyro.infer.autoguide import AutoDiagonalNormal
import pyro.distributions as dist
from scipy.stats import beta, norm

# to import the library biolab_functions and model_execution
sys.path.insert(0, '../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring


class VariationalInference:

    def __init__(self, model=None, prior=None, modify_params=None):
        pyro.clear_param_store()

        self.simulated_model = model

        # init prior params
        self.params_distribution = prior

        # init modify_param for each mbr
        self.modify_params = modify_params

    def model(self, data):
        # sample theta[i] from prior distribution if it is probabilistic
        theta = {}
        for param in self.params_distribution.keys():
            if isinstance(self.params_distribution[param], list):
                # probabilistic
                theta[param] = pyro.sample(f"theta_{param}", dist.Normal(self.params_distribution[param][0],
                                                                         self.params_distribution[param][1])).item()

            else:
                # deterministic
                theta[param] = self.params_distribution[param]

        # check data and compare error (len(data) == MBRs Quantity )
        # for i, mbr_sample in enumerate(data):
        for i in pyro.plate("data_loop", len(data)):

            mbr_sample = data[i]

            # simulate from sample [j-1] to sample[j]
            for j, samples in enumerate(mbr_sample):
                if j != 0:
                    # get data from MBR "i" at sample "j"
                    x0 = [v for k, v in mbr_sample[j - 1].items() if k != 'TP']
                    initial_values = {f'{k}': v for k, v in mbr_sample[0].items() if k != 'TP'}

                    try:
                        # Modify params (apply coeff)
                        for param in self.modify_params[i]:
                            theta[param] = theta[param] * self.modify_params[i][param]

                        obs = self.simulated_model.execute(x0=x0, t=[mbr_sample[j - 1]['TP'], mbr_sample[j]['TP']],
                                                           model_parameters=theta, initial_values=initial_values)

                        # real sample value stored, without 'TP'
                        y = [v for k, v in mbr_sample[j].items() if k != 'TP']

                        # add noise to the observed value with normal distribution
                        for k in range(len(obs[1][-1])):
                            pyro.sample(f"obs_{i}_{j}_{k}", dist.Normal(obs[1][-1][k], 0.0002), obs=torch.tensor(y[k]))

                    except:
                        print("[Variational-Inference] - Error on model execution - continue")
                        continue

    def sample_param(self):
        theta = {}
        # sample theta[i] from prior distribution if it is probabilistic
        for param in self.params_distribution.keys():
            if isinstance(self.params_distribution[param], list):
                # probabilistic
                theta[param] = pyro.sample(f"theta_{param}", dist.Beta(self.params_distribution[param][0],
                                                                       self.params_distribution[param][1])).item()

            else:
                # deterministic
                theta[param] = self.params_distribution[param]
        return theta

    def optimize(self, data, n_steps, lr=0.0005, betas=(0.90, 0.999)):
        # set the optimizer
        # optimizer = Adam({"lr": lr, "betas": betas})
        optimizer = ClippedAdam({"lr": lr})

        my_autoguide = AutoDiagonalNormal(self.model)

        # set inference algorithm
        svi = SVI(self.model, my_autoguide, optimizer, loss=Trace_ELBO())

        losses = []

        # loop from step
        for step in range(n_steps):
            losses.append(svi.step(data))
            if step % 100 == 0:
                print('.', end='')

        # return learned values
        distributions = self.params_distribution.copy()
        for param in self.params_distribution.keys():
            if param == "mu_max":
                loc = pyro.param(my_autoguide._pyro_name + ".loc").detach().numpy()
                scale = pyro.param(my_autoguide._pyro_name + ".scale").detach().numpy()
                distributions[param] = [loc[0], scale[0]]
            if param == "YPX":
                loc = pyro.param(my_autoguide._pyro_name + ".loc").detach().numpy()
                scale = pyro.param(my_autoguide._pyro_name + ".scale").detach().numpy()
                distributions[param] = [loc[1], scale[1]]

        return distributions, losses


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameter modify factor for each mbr",
)
def main(mbrs, modify):
    # set seed for torch and numpy frameworks
    seed = 12345678
    torch.manual_seed(seed=seed)
    pyro.set_rng_seed(seed)
    np.random.seed(seed=seed)

    # Initialize monitoring object
    monitoring = Monitoring("Parameter Distribution Update")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    MBRs = json.loads(mbrs)
    modify_params = np.array(json.loads(modify)).flatten()

    model_parameters = helper.get_params()
    model = ExponentialFedBatch()

    # ---------------------------------- INFERENCE --------------------------------------------------------

    model_parameters['parameters_estimated'][0].update({"mu_max": [0.5, 0.0421], "YPX": [0.1045, 0.0371]})

    distributions = model_parameters['parameters_estimated'][0]

    distributions_step = [distributions]
    losses_step = []
    for sample in range(1, 10):
        data = []
        for mbr in MBRs:
            mbr_measurements = helper.get_measurements(mbr)
            data.append(list([mbr_measurements['samples'][sample - 1], mbr_measurements['samples'][sample]]))
        monitoring.save_message("Running SVI...")
        svi = VariationalInference(model=model, prior=distributions,
                                   modify_params=modify_params)
        distributions, losses = svi.optimize(data=data, n_steps=500)
        distributions_step.append(distributions)
        losses_step.append(losses)

    # ---------------------- PLOT --------------------
    # Plot losses
    plt.figure(figsize=(10, 3), dpi=100).set_facecolor('white')
    plt.plot(np.array(losses_step).flatten())
    plt.xlabel('iters')
    plt.ylabel('loss')
    plt.title('Convergence of SVI')
    plt.show()

    # Plot params
    for param in ["mu_max", "YPX"]:
        plt.figure(f"Parameter '{param}'")

        # plots distributions over iterations
        for i, p in enumerate(distributions_step):
            distribution = p[param]

            x = np.linspace(norm.ppf(0.01, distribution[0], distribution[1]),
                            norm.ppf(0.99, distribution[0], distribution[1]), 100)
            plt.plot(x, norm.pdf(x, distribution[0], distribution[1]), label=f'iteration {i}')

        plt.title(f"Distribution for parameter '{param}'")
        plt.legend()
        plt.show()

    return distributions_step


if __name__ == "__main__":
    main()
