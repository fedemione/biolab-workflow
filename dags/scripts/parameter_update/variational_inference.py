import sys
import json
import numpy as np
import click
import torch
import torch.distributions.constraints as constraints
import pyro
from pyro.optim import Adam
from pyro.infer import SVI, Trace_ELBO
import pyro.distributions as dist

# to import the library biolab_functions and model_execution
sys.path.insert(0, '../../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring


class VariationalInference:

    def __init__(self, model=None, prior=None, guide=None):
        pyro.clear_param_store()

        self.simulated_model = model

        # guide params fixed only for distribution params
        self.distribution_guide = guide

        # init prior params
        self.params_distribution = prior

    def model(self, data):

        # sample theta[i] from prior distribution if it is probabilistic, the other parameters are left as they were
        theta = self.sample_param()

        # check data and compare error (len(data) == MBRs Quantity )
        for mbr in pyro.plate("data_loop", len(data)):

            mbr_sample = data[mbr]["samples"]

            # apply parameters used to the probabilistics ones
            theta = self.unify_params(theta, data[mbr]["params"])

            # simulate from sample [s-1] to sample[s]
            for s, samples in enumerate(mbr_sample):
                if s != 0:
                    # get data from MBR at sample "s"
                    x0 = [val for key, val in mbr_sample[s - 1].items() if key != 'TP']
                    initial_values = {f'{key}': val for key, val in data[mbr]["initial_values"].items() if key != 'TP'}

                    try:
                        # simulate the step [s-1, s]
                        obs = self.simulated_model.execute(x0=x0, t=[mbr_sample[s - 1]['TP'], mbr_sample[s]['TP']],
                                                           model_parameters=theta, initial_values=initial_values)

                        # get the real sample value stored, without 'TP'
                        y = [val for key, val in mbr_sample[s].items() if key != 'TP']

                        # add noise to the observed value with normal distribution
                        for m in range(len(obs[1][-1])):
                            pyro.sample(f"obs_{mbr}_{s}_{m}", dist.Normal(obs[1][-1][m], 0.0002), obs=torch.tensor(y[m]))

                    except:
                        print("[Variational-Inference] - Error on model execution - continue")
                        continue

    def guide(self, data):
        # register two variational params (for each theta) in Pyro (alpha, beta)
        for param in self.distribution_guide.keys():
            alpha_q = pyro.param(f"alpha_q_{param}", torch.tensor(self.distribution_guide[param][0]),
                                 constraint=constraints.positive)
            beta_q = pyro.param(f"beta_q_{param}", torch.tensor(self.distribution_guide[param][1]),
                                constraint=constraints.positive)
            # sample theta
            pyro.sample(f"theta_{param}", dist.Beta(alpha_q, beta_q))

    def sample_param(self):
        theta = {}
        # sample theta[i] from prior distribution if it is probabilistic
        for param in self.params_distribution.keys():
            print(param)
            if isinstance(self.params_distribution[param], list):
                # probabilistic
                theta[param] = pyro.sample(f"theta_{param}", dist.Beta(self.params_distribution[param][0],
                                                                       self.params_distribution[param][1])).item()
            else:
                # deterministic
                theta[param] = self.params_distribution[param]
        return theta

    def unify_params(self, theta, params_used):
        # if the params is deterministic, we change it for the real used one
        for param in self.params_distribution.keys():
            if not isinstance(self.params_distribution[param], list):
                # deterministic
                theta[param] = params_used[param]
        return theta

    def optimize(self, data, n_steps, lr=0.0005, betas=(0.90, 0.999)):
        # set the optimizer
        optimizer = Adam({"lr": lr, "betas": betas})

        # set inference algorithm
        svi = SVI(self.model, self.guide, optimizer, loss=Trace_ELBO())

        # loop from step
        for step in range(n_steps):
            svi.step(data)
            if step % 100 == 0:
                print('.', end='')

        # return learned values
        distributions = self.params_distribution.copy()
        for param in self.distribution_guide.keys():
            alpha_q = round(pyro.param(f"alpha_q_{param}").item(), 3)
            beta_q = round(pyro.param(f"beta_q_{param}").item(), 3)
            distributions[param] = [alpha_q, beta_q]

        return distributions


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameter modify factor for each mbr",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, modify, seed):
    # set seed for torch, pyro and numpy frameworks
    torch.manual_seed(seed=seed)
    pyro.set_rng_seed(seed)
    np.random.seed(seed=seed)

    # Initialize monitoring object
    monitoring = Monitoring("Parameter Distribution Update")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    MBRs = json.loads(mbrs)
    guide = json.loads(unknows)

    model_parameters = helper.get_params()
    data = []
    model = ExponentialFedBatch()

    for mbr in MBRs:
        mbr_measurements = helper.get_measurements(mbr)
        # TODO: obtener los sampleos nuevos (que no sean los ultimos de todos los MBRs, por si no coincide el tamaño de
        # TODO: plato del hamilton con la cantidad de columnas)
        # data.append(mbr_measurements['samples'][::len(mbr_measurements['samples'])-1])

        # Save new observation: last two samples to simulate inside the SVI
        # D = [X, theta, Y]
        data.append({
            "initial_values": mbr_measurements['samples'][0],
            "samples": list([mbr_measurements['samples'][-2], mbr_measurements['samples'][-1]]),
            "params": mbr_measurements['params'][-1]
        })

    # ---------------------------------- LOGGING ---------------------------------------------------------

    monitoring.save_message("Initializing values and bioreactor parameters")
    monitoring.save_message("Guide", guide)
    monitoring.save_message("Prior parameters", model_parameters['parameters_estimated'][-1])

    # ---------------------------------- INFERENCE --------------------------------------------------------

    monitoring.save_message("Running SVI...")
    svi = VariationalInference(model=model, prior=model_parameters['parameters_estimated'][-1], guide=guide)
    distributions = svi.optimize(data, 500)
    monitoring.save_message("Estimation Finished")

    # --------------------------------- SAVE RESULTS ------------------------------------------------------

    # add parameters distributions re-estimated
    model_parameters['parameters_estimated'].append(distributions)

    # save new params
    helper.save_params(model_parameters)
    monitoring.save_message("Save parameters estimated (posterior) to json file", distributions)
    monitoring.save_file()

    return distributions


if __name__ == "__main__":
    main()
