# !/usr/bin/env python
import numpy
import logging
import click
import json
import sys
import numpy as np
from pyfoomb import Caretaker
from pyfoomb import Measurement

# to import the library biolab_functions and model_execution
sys.path.insert(0, '../../..')
from scripts.models.pyfoomb.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import get_params, get_measurements, save_params


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    model_parameters = get_params()
    data = []

    logging.info("Reading measures")
    for mbr in MBRs:
        mbr_measurements = get_measurements(mbr)
        measurements_name = mbr_measurements['samples'][0].keys()

        # init Dict for accumulate measures for each timepoint
        values = {_measure: [] for _measure in measurements_name}
        for sample in mbr_measurements['samples']:
            for _measure in measurements_name:
                values[_measure].append(float(sample[_measure]))

        # save measure and set error 0.1
        for _measure in measurements_name:
            if _measure != 'TP':
                _measurement = Measurement(
                    name=str.upper(_measure),  # p, s, x, vl
                    replicate_id=mbr,  # mbr1, mbr2, mbr3
                    timepoints=values['TP'],  # time when the measure was taken
                    values=values[_measure],  # values
                    errors=[0.0001 for _ in range(len(values[_measure]))]
                )
                data.append(_measurement)

    # add "0" at the end of the measures, because of pyfoomb execution --> "X" --> "X0"
    initial_values = {f'{k}0': v for k, v in mbr_measurements['samples'][0].items() if k != 'TP'}
    caretaker = Caretaker(
        bioprocess_model_class=ExponentialFedBatch,
        model_parameters=model_parameters['parameters_estimated'][-1],
        initial_values=initial_values,
        replicate_ids=MBRs,
    )

    # Calling the method with its default keyword arguments
    estimates, est_info = caretaker.estimate_parallel(
        unknowns=list(_unknows.keys()),
        bounds=list(_unknows.values()),
        measurements=data,
        report_level=3,  # We set the report_level to see some output during the optimization process.
    )
    logging.info("Estimation Finished")

    # duplicate last value
    model_parameters['parameters_estimated'].append(model_parameters['parameters_estimated'][-1].copy())

    # fix decimal length
    estimates = {k: round(v, 3) for k, v in estimates.items()}

    # replace new estimations
    model_parameters['parameters_estimated'][-1].update(estimates.items())

    # save new params
    save_params(model_parameters)

    return estimates.items()


if __name__ == "__main__":
    main()
