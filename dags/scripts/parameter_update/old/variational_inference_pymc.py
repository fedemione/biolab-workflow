import sys
import json
import numpy as np
import click

import arviz as az
import matplotlib.pyplot as plt
import numpy as np
import pymc3 as pm
import theano

# to import the library biolab_functions and model_execution
sys.path.insert(0, '../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameter modify factor for each mbr",
)
def main(mbrs, unknows, modify):
    # set seed for torch and numpy frameworks
    seed = 12345678
    pm.set_tt_rng(seed)
    np.random.seed(seed=seed)

    # Initialize monitoring object
    monitoring = Monitoring("Parameter Distribution Update")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    modify_params = np.array(json.loads(modify)).flatten()

    model_parameters = helper.get_params()
    exp_model = ExponentialFedBatch()

    # ---------------------------------- INFERENCE --------------------------------------------------------

    distributions = model_parameters['parameters_estimated'][0]

    for sample in range(1, 10):
        data = []
        for mbr in MBRs:
            mbr_measurements = helper.get_measurements(mbr)
            # data.append(list([mbr_measurements['samples'][sample - 1], mbr_measurements['samples'][sample]]))
            data.append(mbr_measurements['samples'][-1])
            x0 = [v for k, v in mbr_measurements['samples'][0].items() if k != 'TP']
            initial_values = {f'{k}': v for k, v in mbr_measurements['samples'][0].items() if k != 'TP'}

        with pm.Model() as model:
            theta = {}
            # sample theta[i] from prior distribution if it is probabilistic
            for param in distributions.keys():
                if isinstance(distributions[param], list):
                    # probabilistic
                    theta[param] = pm.Beta(f"theta_{param}", alpha=distributions[param][0],
                                           beta=distributions[param][1])

                else:
                    # deterministic
                    theta[param] = distributions[param]

            # execute biolab model
            y = exp_model.execute(x0=x0, t=[0, 36], model_parameters=theta, initial_values=initial_values)

            with model:
                for y in data:
                    pm.Deterministic("y", y)

    return distributions


if __name__ == "__main__":
    main()
