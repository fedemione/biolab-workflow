# !/usr/bin/env python
import click
import sys
import json
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta

# to import the library biolab_functions
sys.path.insert(0, '../..')
from scripts.lib.biolab_functions import BiolabHelper
from scripts.lib.monitoring import Monitoring
from scripts.models.assimulo.model_execution import ExponentialFedBatch


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--tf",
    type=click.FLOAT,
    required=True,
    help="Final execution time",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameters guide to plot",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(tf, mbrs, unknows, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    # Initialize monitoring object
    monitoring = Monitoring("Plot Results")

    # ---------------------------------- GET DATA ---------------------------------------------------------
    helper = BiolabHelper()
    model_parameters = helper.get_params()
    guide = json.loads(unknows)
    MBRs = json.loads(mbrs)
    measurements = helper.get_measurements(MBRs[0])
    measurements_name = [key for key in measurements['samples'][0].keys() if key != 'TP']
    initial_values = measurements['samples'][0]

    params = model_parameters['parameters_estimated'][-1].copy()

    # check if model parameters are distributions or determined (only the first value, to change all)
    for param in params:
        if isinstance(params[param], list):
            mean = beta.stats(params[param][0], params[param][1], moments='m')
            params[param] = float(mean)

    x0 = [initial_values[measure] for measure in measurements_name]

    # ---------------------------------- SIMULATIONS ---------------------------------------------------------

    # Simulate insilico experiment with 'real parameters' and estimated with mean of parameters distributions calculated
    estimated_simulation = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=params,
                                                         initial_values=initial_values)

    insilico_simulation = ExponentialFedBatch().execute(x0=x0, t=[0, tf],
                                                        model_parameters=model_parameters['parameters_real'],
                                                        initial_values=initial_values)

    with plt.style.context('ggplot'):

        # --------------------------- Plot estimated vs in-silico ---------------------------
        for index, measure in enumerate(measurements_name):
            plt.figure(f"Measure '{measure}'")
            plt.plot(estimated_simulation[0], np.transpose([estimated_simulation[1][:, index]]), label='Estimated')
            plt.plot(insilico_simulation[0], np.transpose([insilico_simulation[1][:, index]]), label="In-silico")
            plt.title(f"Measure '{measure}'")
            plt.xlabel("Time [min]")
            plt.ylabel("Concentration (g/l)")
            plt.legend()
            plt.savefig(fname=f"/data/plots/measure_{measure}.png")
            #plt.show()

        # --------------------------- Plot params distributions ---------------------------

        for param in guide:
            plt.figure(f"Parameter '{param}'")

            # plots guide
            x = np.linspace(beta.ppf(0.01, guide[param][0], guide[param][1]),
                            beta.ppf(0.99, guide[param][0], guide[param][1]), 100)
            plt.plot(x, beta.pdf(x, guide[param][0], guide[param][1]), '--', label='guide', color='purple')

            plt.axvline(x=model_parameters['parameters_real'][param], ls=':', color='teal', label='in-silico param')

            # plots distributions over iterations
            for i, p in enumerate(model_parameters["parameters_estimated"]):
                distribution = p[param]

                x = np.linspace(beta.ppf(0.01, distribution[0], distribution[1]),
                                beta.ppf(0.99, distribution[0], distribution[1]), 100)
                plt.plot(x, beta.pdf(x, distribution[0], distribution[1]), label=f'iteration {i}')

            plt.title(f"Distribution for parameter '{param}'")
            plt.legend()
            plt.savefig(fname=f"/data/plots/parameter_{param}.png")
            #plt.show()

        # -----------------------------------------------------------------------------------

    monitoring.save_file()


if __name__ == "__main__":
    main()
