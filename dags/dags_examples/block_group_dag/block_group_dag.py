from numpy import trunc
import airflow
from airflow.models.dag import DAG
from scripts.lib.biolab_nodes import BiolabNodes

# ------------------------------------- INITIAL DAG ---------------------------------------------------
with DAG(
        dag_id="block_group_dag",
        description="Management of experimental-computational workflows with Apache Airflow, Assimulo and Docker.",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval="@once",
) as dag:
    # instance of the class with the path of the project. You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Asus/Documents/Proyectos/biolab-workflow", dag_id=dag.dag_id)

    # -------------- Initialize parameters, initial values and times form config.cfg file --------------
    mbrs_row = [f"mbr{row}" for row in range(1, Experiment.helper.read_config('MBR')['QTY_R'] + 1)]
    mbrs_col = [f"{col}" for col in range(1, Experiment.helper.read_config('MBR')['QTY_C'] + 1)]
    unknows_estimate = Experiment.helper.read_config('PARAMETER_ESTIMATION_GUIDE')
    unknows_optimization = Experiment.helper.read_config('PARAMETER_OPTIMIZATION_BOUNDS')

    # Set the seed for the entire experiment
    seed = Experiment.helper.read_config('SEED')
    Experiment.set_seed(seed)

    # extract modify params
    modify_params = Experiment.helper.read_config('MODIFY_PARAMS')

    # get sample times
    _, _t_sample = Experiment.helper.calc_times()

    # get hamilton data
    hamilton_plate_size = Experiment.helper.read_config('HAMILTON')['PLATE_SIZE']

    # create bioreactor name row + column
    mbrs = []
    for i in mbrs_row:
        for j in mbrs_col:
            mbrs.append(i + j)

    # ---------------------------------- DAGs Start Node -----------------------------------------
    start = Experiment.start(task_id="Start")

    # init node (blocks) dictionary, hamilton plate samples and variable for las computational block
    node = {}
    hamilton_samples = []
    last_computational_block = None

    # for each sample defined, do an init/update - sample - parameter estimation - parameter optimization LOOP
    for sample, t_s in enumerate(_t_sample):

        if sample == 0:
            node[f"Init_MBRs{sample}"] = Experiment.block_init(mbrs=mbrs)

        # calculate number of column for each sample - Sequential sample C1,C2,C3...
        # the following line generate an index of which column it collects samples. Remind "sample" starts from 0
        column_to_sample = int(sample - len(mbrs_col) * trunc(sample / len(mbrs_col)))

        # create a list of params modifiers from a specific column
        modify_params_col = [item[column_to_sample] for item in modify_params]

        # create a list of bioreactor from a specific column
        mbr_col_list = [i + mbrs_col[column_to_sample] for i in mbrs_row]

        # ------------------------------ block 0 --------------------------------------------------------
        node[f"Block0_Column{column_to_sample + 1}_T{sample + 1}"] = Experiment.block0(
            mbrs_row=mbr_col_list,
            instance=sample + 1,
            column_to_sample=column_to_sample
        )

        # ------------------------------ block 1 --------------------------------------------------------
        node[f"Block1_Column{column_to_sample + 1}_T{sample + 1}"] = Experiment.block1(
            mbrs_row=mbr_col_list,
            instance=sample + 1,
            column_to_sample=column_to_sample,
            modify_params=modify_params_col,
            tf=f"{int(t_s)}"
        )

        # add sample node_id to hamilton plate
        hamilton_samples.append(f"Block1_Column{column_to_sample + 1}_T{sample + 1}")

        # ------------------------------------Dependencies------------------------------------------------
        if sample == 0:
            start >> node[f"Init_MBRs{sample}"] >> \
            node[f"Block0_Column{column_to_sample + 1}_T{sample + 1}"] >> \
            node[f"Block1_Column{column_to_sample + 1}_T{sample + 1}"]
        else:
            last_block >> node[f"Block0_Column{column_to_sample + 1}_T{sample + 1}"] >> \
            node[f"Block1_Column{column_to_sample + 1}_T{sample + 1}"]

        # TODO: delete when PE refer correctly to samples in hamilton plate
        if len(hamilton_samples) == 1 and last_computational_block is not None:
            last_computational_block >> node[f"Block0_Column{column_to_sample + 1}_T{sample + 1}"]

        # set the last node
        last_block = node[f"Block1_Column{column_to_sample + 1}_T{sample + 1}"]

        # --------------------------- Hamilton and Computational Blocks ----------------------------------

        # check samples number to fill the hamilton plate and send to atline analysis
        # TODO: check remaining samples, where hamilton is not filled
        if len(hamilton_samples) == hamilton_plate_size:
            plate_number = int(sample / hamilton_plate_size)
            block_hamilton = Experiment.block_hamilton(instance=sample + 1)

            # add computational block
            block_computational = Experiment.block_computational(
                mbrs=mbrs,
                instance=sample + 1,
                unknows_estimation=unknows_estimate,
                unknows_optimization=unknows_optimization,
                modify_params=modify_params,
                tf=_t_sample[-1]
            )

            # add dependency between sample block and hamilton
            for node_id in hamilton_samples:
                node[node_id] >> block_hamilton

            # add dependency between hamilton and computational block
            # TODO: add sensor between hamilton and computational (when sample analysis it is done)
            block_hamilton >> block_computational

            # add dependency between computational blocks
            if last_computational_block:
                last_computational_block >> block_computational

            # update last computational block
            last_computational_block = block_computational

            # reset hamilton samples
            hamilton_samples = []

    # ------------------------ Show results  -----------------------------------------------
    # Plot simulation with new policy to compare results. Plot params distribution
    show_results = Experiment.show_results(
        task_id="Plot_Results",
        mbrs=mbrs,
        unknows=unknows_estimate,
        tf=f"{int(_t_sample[-1])}"
    )

    last_computational_block >> show_results
