import airflow
from airflow import DAG
from scripts.lib.biolab_nodes import BiolabNodes

with DAG(
        dag_id="offline_redesign_dag",
        description="DAG to optimize some model parameters related to feed management",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval=None,
) as dag:
    # Instance of the class with the path of the project. You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Fede/Documents/Airflow/biolab-airflow")

    Experiment.bayesian_offline_redesign(task_id="Bayesian_offline_redesing_prior")
