<p style='text-align: justify;'>

# LinODEnet details

## Model Inputs and Pre-Processing

The neural network model was designed to make predictions based on 14 variables described in Table B.2. For reasons of numerical stability of the training process, the variables have to be re-scaled and transformed to have zero mean and unit variance. For this purpose, we use a per-variable transformation scheme based on the following 4 transformations: 

$$
\begin{alignat}{5}
&\operatorname{Standardize}\colon &&\mathbb{R} &&\longrightarrow \mathbb{R},\; && x\longmapsto \tfrac{x-\mu}{\sigma} \\
&\operatorname{MinMax}\colon &&[x_{\min},\, x_{\max}] &&\longrightarrow [0, 1],\; && x\longmapsto \tfrac{x -x_{\min}}{x_{\max}-x_{\min}} \\
&\operatorname{Box-Cox}\colon &&[0,\infty) &&\longrightarrow \mathbb{R},\; && x\longmapsto \log(x+c) \\
&\operatorname{Logit}\colon &&[0,1] &&\longrightarrow \mathbb{R},\; && x\longmapsto \log(\tfrac{x+c}{1-(x-c)})
\end{alignat}
$$ 

The quantities $x_{\min}, x_{\max}, \mu$ and $\sigma$
denote the minimum, maximum, mean and standard deviation of the variable
over the training data. For the Box-Cox and Logit transforms, the
constant $`c>0`$ is necessary to deal with boundary values, e.g. $x=0$. It
is chosen so that the transformed data looks as much as possible like a
standard normal distribution after normalization: 

$$
\begin{equation}
c^* = \argmin_{c>0}\, W^1 (p(z), N(0,1) ) 
\qquad \begin{aligned} 
z &= \operatorname{Standardize}(y) 
\\ y &= g_c(x)
\\ x &\sim p^\text{emp}_{D^\text{train}}
\end{aligned}
\end{equation}
$$ 


That is, we choose the $`c>0`$ that minimizes the
Wasserstein-1 distance between the empirical distribution that arises by
transforming the training data $x$ with
$g_c\in\{\text{Box-Cox, Logit\}}$ followed by standardization. Note that
the Wasserstein-1 distance of two univariate distributions can be
computed as
${W^1(p_1,p_2) = \int_{0}^1 |F_1^{-1}(q) - F_2^{-1}(q)|\mathrm{d}q}$,
which in turn can be rewritten as a simple sum in the case when $p_1$ is
an empirical and $p_2$ a normal distribution.


#### Preprocessing Pipeline
Depending on the domain of the variable, we choose a different kind of
processing pipeline.

-   Absolute Scale: Variable lives on the half-open interval $[0, \infty)$, then use Box-Cox + Standardize.

-   Bounded Scale: Variable lives on a bounded interval $[a,b]$, then use MinMax + Logit + Standardize.

-   Standard Scale: Variables lives on interval $(-\infty, +\infty)$, or bounds are fuzzy / not crucial, then simply Standardize

-   Time Scaling: The original data is hours since the start of the experiment. We perform MinMax-Scaling.

<img src="tables/table-b-2.png" alt="Table B.2" width=600/>


Therefore, if $T^\text{raw}$ and $X^\text{raw}$ is the raw data, and
$\varphi$ denotes the element-wise variable preprocessing
transformation, and $\vartheta$ the time-preprocessing, then
Equation (5) in the paper in practice reads:

$$
\begin{equation}
\hat{X} = \varphi^{-1}\bigl(\text{model}(\vartheta(T^\text{raw}), \varphi(X^\text{raw}))\bigr)
\end{equation}
$$

#### Future covariates (controls)
Controls variables (cf. Table B.2) are planned ahead of time and as
such are given to the model as inputs even for future time stamps. For
instance, if we wanted to make a forecast for the next 3 hours given the
observations from the previous 2 hours, then $T$ would contain all time
stamps from `now-2h` until `now+3h`, and $X$ would be a sparse matrix
(filled with `NaN` for missing entries), which contains all variables
for $`now-2h \leq t < now`$ and all planned controls for
$`now \leq t < now+3h`$.


## Model Architecture Details

Table B.3 provides an overview over the model architecture. Additionally, we explain two detailed skipped over in Section 5.3.1.

#### 1. Virtual Channels
As explained in [Model Inputs and Pre-Processing](#model-inputs-and-pre-processing), the model is designed to
use the 14 variables described in
Table B.2. Additionally, the model used in
the experiments tracks 50 so called "hidden" or "virtual" channels. That
is, in filter equation (2) in the paper, $\hat{x}_t$ is actually of size 64,
and every $x_t^\text{obs}$ is a vector of size 14 which is padded by a
constant vector holding 50 `NaN` values before being fed into the
Filter.

The purpose of these virtual channels is to allow the model to use them
as a sort of memory buffer. Since these never observe any actual data,
the model can learn to use them however it seems fit. In our
experiments, we found that adding these virtual channels can slightly
increase performance but also potentially leads to overfitting on the
trained time horizon (e.g. if the model is trained to forecast on 3h
slices, the performs slightly improves within the 3h slice, but the
generalization capability to longer time horizons seems to drop). We
advise practitioners to perform careful hyperparameter optimization to
determine whether to use the virtual channels or not.

#### 2. ReZero Technique
Throughout all model components, we make use of the ReZero technique [(Bachlechner et al., 2021)](https://proceedings.mlr.press/v161/bachlechner21a.html), which dramatically helps to
increase the model stability during training. This simple technique
consists of deactivating all non-linear blocks at the beginning of
training by the introduction of zero-initialized trainable scalars. This
effectively makes all residual components look like identify functions
at initialization time: 

$$
\begin{equation}
    x' = x - \varepsilon f(x) \qquad \text{$\varepsilon$ trainable scalar initialized as $0$}
\end{equation}
$$ 

This can be used as a plug-in method whenever a residual
connection is used, and applies to equations (2), (3) and (4) in the paper. For the system component, we use the variant ${z_{t+\Delta t} = e^{\varepsilon A\Delta t}z_t}$.

<img src="tables/table-b-3.png" alt="Table B.3" width=600/>

</p>